//Exercise - 2.1
// 1. Select all of the div elements that have a class of "module".
$("div.module");
// 2. Come up with three selectors that you could use to get the third item in the #myList unordered list. Which is the best to use? Why?
$("li#myListItem");
$("#myListItem"); 
$("ul#myList li:nth-child(3)");
$("div#main ul#myList li:nth-child(3)") // this is best because it is specific for the element that is needed 
// to get selected and the script won't get disturbed even after adding several redundant elements to html in future.

// 3. Select the label for the search input using an attribute selector.
$('div#main form#search label[for="q"]');
// 4. Figure out how many elements on the page are hidden.
$(":hidden").length;
// 5. Figure out how many image elements on the page have an alt attribute.
$("img[alt]");
// 6. Select all of the odd table rows in the table body.
$("table tbody tr:odd");
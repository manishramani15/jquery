// 1. Add five new list items to the end of the unordered list #myList.
for(var i = 0; i < 5; i++) {
  $("ul#myList li:last").append("<li>");
}
// 2. Remove the odd list items
$("ul#myList li:odd").remove();// if remove odd list items from #myList.
$("li:odd").remove(); //if remve odd list items from whole body.
// 3. Add another h2 and another paragraph to the last div.module
$("div.module:last").append("<h2>").append("<p>");
// 4. Add another option to the select element; give the option the value "Wednesday".
$("select").append("<option value='Wednesday'>");
// 5. Add a new div.module to the page after the last one; put a copy of one of the existing images inside of it.
$("div.module:last").append("<div class='module'>");
$("img:last").appendTo("div.module:last");